﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_JC_examen.DB;

namespace T3_JC_examen.Controllers
{
    public class EjerciciosController : Controller
    {
        private RutinaContext context;
        public EjerciciosController(RutinaContext context)
        {
            this.context = context;
        }

        public IActionResult Index()
        {
            var Ejercicios = context.Ejercicios.ToList();
            return View(Ejercicios);
        }
        public IActionResult Detail(int id)
        {
            var DetalleEjercicios = context.Ejercicios.Where(o => o.Id == id).FirstOrDefault();
            return View(DetalleEjercicios);
        }
    }
}

