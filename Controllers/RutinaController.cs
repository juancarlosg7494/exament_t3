﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_JC_examen.DB;
using T3_JC_examen.Estrategia;
using T3_JC_examen.Models;

namespace T3_JC_examen.Controllers
{
    public class RutinaController : Controller
    {
        private RutinaContext context;
        private TipoRutina tipoRutina;
        public RutinaController(RutinaContext context)
        {
            this.context = context;
        }
        public ActionResult Index()
        {
            var usuario = GetUsuario();
            var rutinas = context.Rutinas.Where(o => o.UsuarioId == usuario.Id);
            return View(rutinas);
        }

        public ActionResult DetalleRutina(int id)
        {
            var usuario = GetUsuario();
            var ejercicioRutinas = context.RutinaEjercicios.Where(o => o.RutinaId == id).Include(o => o.Ejercicio).ToList();

            return View(ejercicioRutinas);
        }

        [HttpGet]
        public ActionResult Create()
        {
            ViewBag.usuario = GetUsuario();
            return View(new Rutina());
        }

        [HttpPost]
        public ActionResult Create(Rutina rutina)
        {
            if (ModelState.IsValid)
            {
                switch (rutina.Tipo)
                {
                    case "Principiante":
                        tipoRutina = new Principiante();
                        break;
                    case "Intermedio":
                        tipoRutina = new Intermedio();
                        break;
                    case "Avanzado":
                        tipoRutina = new Avanzado();
                        break;
                }
                var ejerciciosSeleccionados = tipoRutina.Ejercicios(context);

                List<RutinaEjercicio> ListaEjercicios = new List<RutinaEjercicio>();
                foreach (var ejercicio in ejerciciosSeleccionados)
                {
                    ListaEjercicios.Add(new RutinaEjercicio() { EjercicioId = ejercicio.Id, Tiempo = CalcularTiempo(rutina.Tipo) });
                }
                rutina.Ejercicios = ListaEjercicios;
                context.Rutinas.Add(rutina);
                context.SaveChanges();
                return RedirectToAction("Index");
            }

            return View("Create", rutina);
        }
        private Usuario GetUsuario()
        {
            var claim = HttpContext.User.Claims.FirstOrDefault();
            if (claim != null)
            {
                var user = context.Usuarios.Where(o => o.Username == claim.Value).FirstOrDefault();
                return user;
            }
            return null;
        }

        public int CalcularTiempo(string tipo)
        {
            int tiempo;
            Random random = new Random();
            if (tipo == "Avanzado")
            {
                tiempo = 120;
            }
            else
            {
                tiempo = random.Next(60, 120);
            }
            return tiempo;
        }

    }
}
