﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace T3_JC_examen.Models
{
    public class Rutina
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Este campo es obligatorio")]
        public string Tipo { get; set; }
        public int UsuarioId { get; set; }
        public List<RutinaEjercicio> Ejercicios { get; set; }
    }
}
