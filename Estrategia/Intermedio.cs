﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_JC_examen.DB;
using T3_JC_examen.Models;

namespace T3_JC_examen.Estrategia
{
    public class Intermedio : TipoRutina
    {
        public override List<Ejercicio> Ejercicios(RutinaContext context)
        {
            var rnd = new Random();
            var listaEjercicios = context.Ejercicios.ToList();
            var rutina = listaEjercicios.OrderBy(item => rnd.Next()).Take(10).ToList();
            return rutina;
        }
    }
}
