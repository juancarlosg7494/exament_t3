﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using T3_JC_examen.DB;
using T3_JC_examen.Models;

namespace T3_JC_examen.Estrategia
{
    public abstract class TipoRutina
    {
        public abstract List<Ejercicio> Ejercicios(RutinaContext context);
    }
}
