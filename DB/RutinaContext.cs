﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using T3_JC_examen.Models;
using T3_JC_examen.DB.Mapping;

namespace T3_JC_examen.DB
{
    public class RutinaContext : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Ejercicio> Ejercicios { get; set; }
        public DbSet<Rutina> Rutinas { get; set; }
        public DbSet<RutinaEjercicio> RutinaEjercicios { get; set; }

        public RutinaContext(DbContextOptions<RutinaContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new UsuarioMap());
            modelBuilder.ApplyConfiguration(new EjercicioMap());
            modelBuilder.ApplyConfiguration(new RutinaEjercicioMap());
            modelBuilder.ApplyConfiguration(new RutinaMap());


        }
    }
}
