﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using T3_JC_examen.Models;

namespace T3_JC_examen.DB.Mapping
{
    public class RutinaEjercicioMap : IEntityTypeConfiguration<RutinaEjercicio>
    {
        public void Configure(EntityTypeBuilder<RutinaEjercicio> builder)
        {
            builder.ToTable("RutinaEjercicio");
            builder.HasKey(o => o.Id);
            builder.HasOne(o => o.Ejercicio).
                WithMany().
                HasForeignKey(o => o.EjercicioId);

        }
    }
}
